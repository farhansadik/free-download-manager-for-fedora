# Free Download Manager for Fedora

## Installation 
1. Clone git repository: 
```bash
git clone https://gitlab.com/farhansadik/free-download-manager-for-fedora
```
2. Redirect to directory and run installer 
```bash
./install 
```

## Official Browser Extensions
You must install browser extensions to use fdm. 

* [Google Chrome](https://chrome.google.com/webstore/detail/free-download-manager-chr/ahmpjcflkgiildlgicmcieglgoilbfdp/RK%3D2/RS%3DfQzscgKGm9aMezJJ7Gf2Fcc.JSo-)
* [Mozilla Firefox](https://addons.mozilla.org/en-US/firefox/addon/free-download-manager-addon/)

## Uninstall
To uninstall run the uninstaller script
```bash
./uninstall
```

## Images 
![](images/1.png)
![](images/2.png)
![](images/3.png)
![](images/4.png)

### Sources <br>
<https://www.freedownloadmanager.org/download-fdm-for-linux.htm>
